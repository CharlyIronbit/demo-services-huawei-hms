package mx.com.procesar.demo.huawei.maps;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.huawei.hms.maps.HuaweiMap;
import com.huawei.hms.maps.MapView;
import com.huawei.hms.maps.MapsInitializer;
import com.huawei.hms.maps.OnMapReadyCallback;
import com.huawei.hms.maps.model.LatLng;
import com.huawei.hms.maps.model.Marker;
import com.huawei.hms.maps.model.MarkerOptions;

import java.util.List;
import java.util.Map;

import mx.com.procesar.demo.huawei.R;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback,
        HuaweiMap.OnMarkerClickListener {
    private static final String TAG = "MapsActivity";
    private static final String MAPVIEW_BUNDLE_KEY = "MapViewBundleKey";

    private HuaweiMap huaweiMap;
    private Marker mMarker;
    private MapView mMapView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MapsInitializer.setApiKey("CgB6e3x9PtxfX1qVcqqdg/FLiVxspK9h8FW8cdMs86lCebzdxAMEHO9+87RIUtizNLc53Nnu73+LQ8gDnDxNhEcV");
        setContentView(R.layout.activity_maps);
        mMapView = findViewById(R.id.mapView);

        Bundle mapViewBundle = null;
        if (savedInstanceState != null) {
            mapViewBundle = savedInstanceState.getBundle(MAPVIEW_BUNDLE_KEY);
        }

        mMapView.onCreate(mapViewBundle);

        mMapView.getMapAsync(this);
    }

    @Override
    public void onMapReady(HuaweiMap map) {
        huaweiMap = map;
        huaweiMap.setMyLocationEnabled(true);// Enable the my-location overlay.
        huaweiMap.getUiSettings().setMyLocationButtonEnabled(true);// Enable the my-location icon.
        createMarker();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        Bundle mapViewBundle = outState.getBundle(MAPVIEW_BUNDLE_KEY);
        if (mapViewBundle == null) {
            mapViewBundle = new Bundle();
            outState.putBundle(MAPVIEW_BUNDLE_KEY, mapViewBundle);
        }

        mMapView.onSaveInstanceState(mapViewBundle);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mMapView.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mMapView.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        if (marker != null && marker.getTag() != null) {
            String idMarker = (String) marker.getTag();
        }
        return false;
    }

    private void createMarker(){
        huaweiMap.clear();
        huaweiMap.setOnMarkerClickListener(this);

        for (Map.Entry<String, List<Double>> entry : DataMap.markets().entrySet()) {
            LatLng latLngMarker = new LatLng(entry.getValue().get(0), entry.getValue().get(1));
            addMarker(latLngMarker, entry.getKey());
        }
    }

    public void addMarker(LatLng latLngMarker, String tag) {
        MarkerOptions options = new MarkerOptions()
                .position(latLngMarker)
                .title("Bienvenido a")
                .snippet(tag);
        huaweiMap.addMarker(options).setTag(tag);
    }

}