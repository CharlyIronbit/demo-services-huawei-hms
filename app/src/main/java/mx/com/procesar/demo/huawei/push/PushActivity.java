package mx.com.procesar.demo.huawei.push;

import static mx.com.procesar.demo.huawei.util.PreferencesHelper.TOKEN_HUAWEI;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;

import com.huawei.agconnect.applinking.AGConnectAppLinking;
import com.huawei.agconnect.config.AGConnectServicesConfig;
import com.huawei.hms.aaid.HmsInstanceId;
import com.huawei.hms.common.ApiException;

import mx.com.procesar.demo.huawei.R;
import mx.com.procesar.demo.huawei.util.PreferencesHelper;

public class PushActivity extends AppCompatActivity {
    private static final String TAG = "PushActivity";

    private PreferencesHelper preferences;
    private AppCompatEditText mTokenPush;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_push);
        mTokenPush = findViewById(R.id.et_token_push);
        preferences = new PreferencesHelper(this);
        getToken();
        getAppLinking();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Uri data = intent.getData();
        Bundle mBundle = intent.getExtras();
        Log.i("TESTER", "onNewIntent " + "\n" +
                "data :" + data + "\n" +
                "bundle :" + mBundle
        );


    }

    private void getToken() {
        new Thread() {
            @Override
            public void run() {
                try {
                    String appId = AGConnectServicesConfig.fromContext(PushActivity.this).getString("client/app_id");
                    String token = HmsInstanceId.getInstance(PushActivity.this).getToken(appId, "HCM");
                    preferences.saveString(TOKEN_HUAWEI, token);
                    Log.i(TAG, "Get Token: " + token);
                    Log.i(TAG, "Get Token: " + preferences.contains(TOKEN_HUAWEI));
                    if(!TextUtils.isEmpty(token)) {
                        Log.i(TAG, "Sending Token To Server. Token:" + token);
                    }
                } catch (ApiException e) {
                    Log.e(TAG, "Get Token Failed, " + e);
                }
            }
        }.start();
        viewToken();
    }

    private void getAppLinking(){
        AGConnectAppLinking.getInstance().getAppLinking(this).addOnSuccessListener(resolvedLinkData -> {
            Uri deepLink = null;
            if (resolvedLinkData!= null) {
                deepLink = resolvedLinkData.getDeepLink();

                String host = deepLink.getHost();
                String path = deepLink.getPath();
                String method = deepLink.getQueryParameter("method");
                String ukey = deepLink.getQueryParameter("ukey");
                String navkey = deepLink.getQueryParameter("navkey");
                String ius = deepLink.getQueryParameter("ius");

                Log.i(TAG, "Datos  -->  \n" +
                        "host  --> " + host + "\n" +
                        "path  --> " + path + "\n" +
                        "method  --> " + method + "\n" +
                        "ukey  --> " + ukey + "\n" +
                        "navkey  --> " + navkey + "\n" +
                        "ius  --> " + ius + "\n"
                );
            }
            Log.i(TAG, "AGConnectAppLinking --> " + deepLink);
        });
    }

    private void viewToken(){
        String token = preferences.getString(TOKEN_HUAWEI);
        mTokenPush.setText(token);
    }
}