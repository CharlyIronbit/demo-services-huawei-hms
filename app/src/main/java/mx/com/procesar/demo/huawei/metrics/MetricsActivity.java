package mx.com.procesar.demo.huawei.metrics;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.content.Intent;
import android.icu.text.SimpleDateFormat;
import android.os.Bundle;
import android.view.View;

import com.huawei.hms.analytics.HiAnalytics;
import com.huawei.hms.analytics.HiAnalyticsInstance;
import com.huawei.hms.analytics.HiAnalyticsTools;

import java.util.Date;
import java.util.Locale;

import mx.com.procesar.demo.huawei.MainActivity;
import mx.com.procesar.demo.huawei.R;
import mx.com.procesar.demo.huawei.crashlytics.CrashlyticsActivity;
import mx.com.procesar.demo.huawei.maps.MapsActivity;
import mx.com.procesar.demo.huawei.push.PushActivity;

public class MetricsActivity extends AppCompatActivity {

    private HiAnalyticsInstance instance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_metrics);

        initiateAnalyticsKit();

        AppCompatButton btnGenerateMetrics01 = findViewById(R.id.btn_metrics_01);
        AppCompatButton btnGenerateMetrics02 = findViewById(R.id.btn_metrics_02);

        btnGenerateMetrics01.setOnClickListener(mOnClickListener);
        btnGenerateMetrics02.setOnClickListener(mOnClickListener);
    }

    private final View.OnClickListener mOnClickListener = v -> {
        switch (v.getId()){
            case R.id.btn_metrics_01:
                reportAnswerEvt("si");
                break;
            case R.id.btn_metrics_02:
                reportAnswerEvt("no");
                break;
            default:
                break;
        }
    };

    private void  initiateAnalyticsKit(){
        HiAnalyticsTools.enableLog();
        instance = HiAnalytics.getInstance(this);
    }

    private void reportAnswerEvt(String answer) {
        Bundle bundle = new Bundle();
        bundle.putString("answer",answer);
        bundle.putString("question", "si funciona??");

        instance.onEvent("tester", bundle);
    }
}