package mx.com.procesar.demo.huawei.crashlytics;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import com.huawei.agconnect.crash.AGConnectCrash;

import mx.com.procesar.demo.huawei.R;

public class CrashlyticsActivity extends AppCompatActivity {
    private static final String TAG = "CrashlyticsActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crashlytics);

        AppCompatButton btnGenerateCrash = findViewById(R.id.btn_generate_crash);
        AppCompatButton btnGenerateException = findViewById(R.id.btn_generate_exception);
        AppCompatButton btnGenerateReport = findViewById(R.id.btn_generate_report);

        btnGenerateCrash.setOnClickListener(mOnClickListener);
        btnGenerateException.setOnClickListener(mOnClickListener);
        btnGenerateReport.setOnClickListener(mOnClickListener);
    }

    private final View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.btn_generate_crash:
                    AGConnectCrash.getInstance().testIt(CrashlyticsActivity.this);
                    break;
                case R.id.btn_generate_exception:
                    try{
                        throw new Exception();
                    }catch (Exception e){
                        AGConnectCrash.getInstance().recordException(e);
                    }
                    break;
                case R.id.btn_generate_report:
                    AGConnectCrash.getInstance().setUserId("testuser");
                    AGConnectCrash.getInstance().testIt(CrashlyticsActivity.this);
                    AGConnectCrash.getInstance().log(Log.DEBUG,"set debug log.");
                    AGConnectCrash.getInstance().log(Log.INFO,"set info log.");
                    AGConnectCrash.getInstance().log(Log.WARN,"set warning log.");
                    AGConnectCrash.getInstance().log(Log.ERROR,"set error log.");
                    AGConnectCrash.getInstance().setCustomKey("stringKey", "Hello world");
                    AGConnectCrash.getInstance().setCustomKey("booleanKey", false);
                    AGConnectCrash.getInstance().setCustomKey("doubleKey", 1.1);
                    AGConnectCrash.getInstance().setCustomKey("floatKey", 1.1f);
                    AGConnectCrash.getInstance().setCustomKey("intKey", 0);
                    AGConnectCrash.getInstance().setCustomKey("longKey", 11L);
                    break;
                default:
                    break;
            }
        }
    };
}